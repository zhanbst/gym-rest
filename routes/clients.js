var express = require('express');
var router = express.Router();
var clients = require('../models/clientsModel');

// read
router.get('/', function(req, res) {
    res.responseType = 'text';
    clients.select(req.body, res);
});

// create
router.post('/', function(req, res) {
    clients.create(req.body, res);
});

// update
router.put('/', function(req, res) {
    clients.update(req.body, res);
});

// delete
router.delete('/:id', function(req, res) {
    console.log(req.params.id);
    clients.delete(req.params.id, res);
});

module.exports = router;