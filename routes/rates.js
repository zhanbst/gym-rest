var express = require('express');
var router = express.Router();
var rates = require('../models/ratesModel');

// read
router.get('/', function(req, res) {
    res.responseType = 'text';
    rates.select(req.body, res);
});

// create
router.post('/', function(req, res) {
    rates.create(req.body, res);
    console.log(req.body);
});

// update
router.put('/', function(req, res) {
    rates.update(req.body, res);
});

// delete
router.delete('/:id', function(req, res) {
    console.log(req.params.id);
    rates.delete(req.params.id, res);
});

module.exports = router;