/**
 * Created by zhanik on 11.11.16.
 */
var express = require('express');
var router = express.Router();
var gyms = require('../models/gymsModel');

// read
router.get('/', function(req, res) {
    res.responseType = 'text';
    gyms.select(req.body, res);
});

// create
router.post('/', function(req, res) {
    gyms.create(req.body, res);
    //console.log(req.body);
});


module.exports = router;