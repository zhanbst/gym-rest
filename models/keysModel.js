var db = require('../DB');

function keys() {
    // SELECT KEY
    this.select = function(req, res) {
        var sql = 'CALL KEYS_SELECT()';
        db.query(sql, function (err, result) {
            res.responseType = 'text';
            res.json(result[0]);
        });
    };

    // CREATE NEW KEY
    this.create = function(req, res) {
        var sql = 'CALL KEYS_CREATE(?)';
        db.query(sql, [req.name], function (err, result) {
            if (err) {
                console.log(err);
            }
            else {
                res.responseType = 'text';
                res.json(result[0]);
            }
        });
    };

    // UPDATE KEY
    this.update = function(req, res) {
        var sql = 'CALL KEYS_UPDATE(?, ?)';
        db.query(sql, [req.id, req.name], function (err, result) {
            if (err) {
                console.log(err);
            }
            else {
                res.send("DONE");
            }
        });
    };

    // DELETE KEY
    this.delete = function(id, res) {
        var sql = 'CALL KEYS_DELETE(?)';
        db.query(sql, [id], function (err, result) {
            if (err) {
                console.log(err);
            }
            else {
                res.send("DONE");
            }
        });
    };
}

module.exports = new keys();