var db = require('../DB');

function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" + twoDigits(1 + this.getUTCMonth()) + "-" + twoDigits(this.getUTCDate()) + " " + twoDigits(this.getHours()) + ":" + twoDigits(this.getUTCMinutes()) + ":" + twoDigits(this.getUTCSeconds());
};


function rates() {
    // SELECT RATES
    this.select = function(req, res) {
        var sql = 'CALL Z_RATES_SELECT()';
        db.query(sql, function (err, result) {
            res.responseType = 'text';
            res.json(result[0]);
        });
    };

    // CREATE NEW RATE
    this.create = function(req, res) {
        var sql = 'CALL Z_RATES_CREATE(?, ?, ?, ?, ?, ?, ?, ?, ?)';
        var sendData = [req.name,
            req.price,
            req.gymid,
            req.training,
            req.starttime,
            req.endtime,
            JSON.stringify(req.weekdays),
            req.enddate,
            req.count];
        console.log(sendData);
        db.query(sql, sendData , function (err, result) {
            if (err) {
                console.log(err);
            }
            else {
                res.responseType = 'text';
                res.json(result[0]);
            }
        });
    };

    // UPDATE RATE
    this.update = function(req, res) {
        var sql = 'CALL Z_RATES_UPDATE(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
        db.query(sql, [req.id, req.name, req.price,
            req.gymid,
            req.training,
            req.starttime,
            req.endtime,
            JSON.stringify(req.weekdays),
            req.enddate,
            req.count], function (err, result) {
            if (err) {
                console.log(err);
            }
            else {
                res.send("DONE");
            }
        });
    };

    // DELETE INSTRUCTOR
    this.delete = function(id, res) {
        var sql = 'CALL Z_RATES_DELETE(?)';
        db.query(sql, [id], function (err, result) {
            if (err) {
                console.log(err);
            }
            else {
                res.send("DONE");
            }
        });
    };
}

module.exports = new rates();

