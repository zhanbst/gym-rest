/**
 * Created by zhanik on 11.11.16.
 */
var db = require('../DB');

function gyms() {
    // SELECT KEY
    this.select = function(req, res) {
        var sql = 'CALL Z_GYMS_SELECT()';
        db.query(sql, function (err, result) {
            res.responseType = 'text';
            res.json(result[0]);
        });
    };

    // CREATE NEW KEY
    this.create = function(req, res) {
        var sql = 'CALL Z_GYMS_CREATE(?)';
        db.query(sql, [req.name], function (err, result) {
            if (err) {
                console.log(err);
            }
            else {
                res.responseType = 'text';
                res.json(result[0]);
            }
        });
    };
}

module.exports = new gyms();