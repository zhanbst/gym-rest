var db = require('../DB');

function clients() {
    // SELECT CLIENTS
    this.select = function(req, res) {
        var sql = 'CALL CLIENTS_SELECT()';
        db.query(sql, function (err, result) {
            res.responseType = 'text';
            res.json(result[0]);
        });
    };

    // CREATE NEW CLIENT
    this.create = function(req, res) {
        console.log(req);
        var sql = 'CALL CLIENTS_CREATE(?, ?, ?, ?)';
        db.query(sql, [req.name, req.address, req.phone, req.email], function (err, result) {
            if (err) {
                console.log(err);
            }
            else {
                res.responseType = 'text';
                res.json(result[0]);
            }
        });
    };

    // UPDATE CLIENT
    this.update = function(req, res) {
        var sql = 'CALL CLIENTS_UPDATE(?, ?, ?)';
        db.query(sql, [req.id, req.name, req.price], function (err, result) {
            if (err) {
                console.log(err);
            }
            else {
                res.send("DONE");
            }
        });
    };

    // DELETE CLIENT
    this.delete = function(id, res) {
        var sql = 'CALL CLIENTS_DELETE(?)';
        db.query(sql, [id], function (err, result) {
            if (err) {
                console.log(err);
            }
            else {
                res.send("DONE");
            }
        });
    };
}

module.exports = new clients();