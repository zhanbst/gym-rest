module.exports = {
    //change to settings.json
    //mysql database credentials
    database: {
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database: 'tsadb_gym_dev',
        multipleStatements: true
    },

    // jwt secret key
    jwtSecret: 'gym'
};